const userModel = require('../models/userModel.js');
var uuid = require('uuid');
const {rooms, getRoom, addUserInRoom, removeUserFromRoom, startGame} = require('../game/game');

module.exports = {

    getRoom: function (req, res) {

        if(rooms){
            const room = getRoom(req.params.id);
            if(room)
                return res.json({message:'success', room:room});
        }
        return res.json({message:'Room with that id was not found.'});   

    },

    getRooms: function (req, res) {

        if(rooms)
            return res.json({message:'success', rooms:rooms});
    },

    startGame: function (req, res) {

        if(rooms){

            let rs = startGame(req.body.roomId, req.body.userId);

            if(rs.error == null)
                return res.json({message:'success', room:rs.room});
        }

        return res.json({message:'Unknown error.'});  
    },

    addRoom: function (req, res) { 

        if(!req.body.name)
            return res.status(500).json({message:"Room name is missing."});
        else{
            
            userModel.findOne({_id: req.body.userId}).exec(function (err, user) {

                if (err) 
                    return res.status(500).json({message:"Unknown error."});

                const room = {
                    _id: uuid.v4(),
                    name: req.body.name,
                    users: [{
                        _id: user._id,
                        username: user.username,
                        positions: [],
                        color: null,
                        firstDiceValue: 0,
                    }],
                    currentPlayer: null,
                    currentPlayerRollsLeft: 0,
                    canCurrentPlayerRollDice: true,
                    canCurrentPlayerMoveFigure: false,
                    currentDiceValue: 1,
                    timeCountDown: 30,
                    enoughPlayersToStart: false,
                    hasGameStarted: false,
                    gameOver: false
                }

                //Add new room in array of rooms
                rooms.push(room);

                return res.json({message:'success', room:room});
            });
        }
        
    },

    addPlayerInRoom: function (req, res) {

        userModel.findOne({_id: req.body.userId}).exec(function (err, user) {

            if (err) 
                return res.status(500).json({message:"Unknown error."});

            if(rooms){
                let rs = addUserInRoom(user, req.body.userId, req.body.roomId);
    
                if(rs.error == null)
                    return res.json({message:'success'});
                else
                return res.json({message: rs.error});
            }
            return res.json({message:'Unknown error.'});
        });

    },

    removePlayerFromRoom: function (req, res) {

        if(rooms){
            let rs = removeUserFromRoom(req.body.userId, req.body.roomId);

            if(rs.error == null)
                return res.json({message:'success'});
        }
        return res.json({message:'Unknown error.'});
    },

    updateRoom: function (req, res) {

       
    },

    deleteRoom: function (req, res) {

        
    },

};