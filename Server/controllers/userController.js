const userModel = require('../models/userModel.js');
const jwt = require('jsonwebtoken');

module.exports = {

    getUser: function (req, res) {
        
        userModel.findOne({_id: req.params.id}).exec(function (err, user) {
            if (err) 
                return res.status(500).json({message:"Error getting user from db."});
            if (!user)
                return res.status(500).json({message:"That user does not exist."});

            res.json({message:'success', user:user});
        });
    },

    getUsers: function (req, res) {
      
    },

    registerUser: function (req, res) {

        if(!req.body.email || !req.body.username || !req.body.password) {
            return res.status(500).json({message:"Not all required fields are filled."});
        }
        else if(req.body.password.length < 8) {
            return res.status(500).json({message:"Password should have at least 8 characters."});
        }

        userModel.findOne({username: req.body.username}).exec(function (err, user) {
            if (err) {
                return res.status(500).json({message:"Unknown error."});
            }
            else if (user){
                return res.status(500).json({message:"That username is already used."});
            }
        });

        const user = new userModel({
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
            points: 0,
            gamesPlayedNum: 0,
            winsNum: 0
        });

        user.save(function (err, user) {
            if (err) {
                return res.status(500).json({message:"Error saving user in db."});
            }
            
            jwt.sign({user: {_id: user._id, username: user.username}}, 'drawAndGuess', {expiresIn: '10h'}, (err, token) =>{
                res.json({message:'success', token:token, user:user});
            });
        });
    },

    loginUser: function (req, res) {

        userModel.authenticate(req.body.username, req.body.password, function (err, user) {
            if (!user) {
                res.status(500).json({message:'Wrong username or password.'});
            }
            else if(err){
                res.status(500).json({message:'Unknown error.'});
            }
            else {
                jwt.sign({user: {_id: user._id, username: user.username}}, 'drawAndGuess', {expiresIn: '10h'}, (err, token) =>{
                    res.json({message:'success', token:token, user:user});
                });
            }
        })
    },

    updateUser: function (req, res) {

       
    },

    deleteUser: function (req, res) {

        
    },

};
