var uuid = require('uuid');
const userModel = require('../models/userModel.js');

//Array of all active rooms
const rooms = [
    {
        _id: uuid.v4(),
        name: 'Default Room',
        users: [],
        currentPlayer: null,
        currentPlayerRollsLeft: 0,
        canCurrentPlayerRollDice: true,
        canCurrentPlayerMoveFigure: false,
        currentDiceValue: 1,
        timeCountDown: 30,
        enoughPlayersToStart: false,
        hasGameStarted: false,
        gameOver: false
    },
];

const PlayerColor = {
    RED: 'red',
    GREEN: 'green',
    YELLOW: 'yellow',
    BLUE: 'blue'
}

const gamePath = [
    [4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [3, 4], [2, 4], [1, 4], [0, 4], [0, 5],
    [0, 6], [1, 6], [2, 6], [3, 6], [4, 6], [4, 7], [4, 8], [4, 9], [4, 10], [5, 10],
    [6, 10], [6, 9], [6, 8], [6, 7], [6, 6], [7, 6], [8, 6], [9, 6], [10, 6], [10, 5],
    [10, 4], [9, 4], [8, 4], [7, 4], [6, 4], [6, 3], [6, 2], [6, 1], [6, 0], [5, 0],
];

//Controll Rooms
const getRoom = (roomId) => {

    const room = rooms.find((room) => room._id === roomId);

    if(!room) return { error: 'Room not found.' };
    return room;
}

const addUserInRoom = (user, userId, roomId) => {

    const room = getRoom(roomId);

    if(room.error){
        return { error: room.error }
    }

    if(!room.hasGameStarted) {     
        console.log("Add new user in room.");

        const newUser = {
            _id: userId,
            username: user.username,
            positions: [],
            color: null,
            firstDiceValue: 0,
        }

        room.users.push(newUser);

        return { error: null, userRoom: room, gameStarted: room.hasGameStarted};
    } 
    else {
        return { error: 'Game has already started. New players can not be added.' }
    }
};

const removeUserFromRoom = (userId, roomId) => {

    const room = getRoom(roomId);

    if(room.hasGameStarted) return { error: 'Game has started. Cannot remove players' }

    const userIndex = room.users.findIndex((u) => u._id.toString() === userId.toString());

    if(userIndex > -1){
        room.users.splice(userIndex, 1);
        return { error: null };
    } 
    else {
        return { error: "User is not in room" };
    }
}

const startGame = (roomId, userId) => {
    console.log("Starting the game.");

    const room = getRoom(roomId);
    let userIndex;

    if(room.users)
        userIndex = room.users.findIndex((u) => u._id.toString() === userId);

    if(userIndex === -1) 
        return { error: 'User is not in room.' };

    if(room.users && room.users.length < 2) 
        return { error: 'Not enough players' };

    room.hasGameStarted = true;

    return { error: null, room: room };
};

const removeRoom = (roomId) => {
    const roomIndex = rooms.findIndex((r) => r._id.toString() === roomId.toString());

    if(roomIndex > -1)
        rooms.splice(roomIndex, 1)
}




//Controll Playing Field
const getInitialsPositions = (playerColor) => {
    switch (playerColor) {
        case PlayerColor.RED:
            return [[0,0], [0,1], [1, 0], [1,1]];
        case PlayerColor.GREEN:
            return [[0,9], [0,10], [1, 9], [1,10]];
        case PlayerColor.BLUE:
            return [[9,0], [9,1], [10, 0], [10,1]];
        case PlayerColor.YELLOW:
            return [[9,9], [9,10], [10, 9], [10,10]];
    }
};

const getStartPosition = (playerColor) => {
    switch (playerColor) {
        case PlayerColor.RED:
            return [4, 0];
        case PlayerColor.GREEN:
            return [0, 6];
        case PlayerColor.BLUE:
            return [10, 4];
        case PlayerColor.YELLOW:
            return [6, 10];
    }
};

const getHomePositions = (playerColor) => {
    switch (playerColor) {
        case PlayerColor.RED:
            return [[5,1], [5,2], [5, 3], [5, 4]];
        case PlayerColor.GREEN:
            return [[1, 5], [2, 5], [3, 5], [4, 5]];
        case PlayerColor.BLUE:
            return [[9, 5], [8, 5], [7, 5], [6, 5]];
        case PlayerColor.YELLOW:
            return [[5, 9], [5, 8], [5, 7], [5, 6]];
    }
};

const getIndexOfPointInPath = (array, point) => {
    if(!array || !point) return -1;
    let index = -1;

    array.map((p, idx) => {
        if(p[0] === point[0] && p[1] === point[1]){
            index = idx;
        }
    })
    return index;
};

const PlayerPath = (playerColor) => {
    let playerPath = [];

    const startIndex = getIndexOfPointInPath(gamePath, getStartPosition(playerColor));

    for(let i = startIndex; i < gamePath.length; i++){
        playerPath.push(gamePath[i]);
    }
    for(let i = 0; i < startIndex; i++){
        playerPath.push(gamePath[i]);
    }
    playerPath = playerPath.concat(getHomePositions(playerColor));

    return playerPath;
};




//Controll Game
const handleGame = (socket, io, roomId) => {
    setTimeout(()=> {
        const room = getRoom(roomId);

        room.users.map((player, playerIndex) => {
            
            switch (playerIndex) {
                case 0:
                    player.color = PlayerColor.RED;
                    break;
                case 1:
                    player.color = PlayerColor.GREEN;
                    break;
                case 2:
                    player.color = PlayerColor.BLUE;
                    break;
                case 3:
                    player.color = PlayerColor.YELLOW;
                    break;
            }

            //dobi začetne pozicije
            player.positions = getInitialsPositions(player.color);
        })
        room.canCurrentPlayerRollDice = true;
        room.currentPlayer = room.users[0];
        room.currentDiceValue = 5;
        
        //send state to room
        io.to(room._id).emit('gameUpdate', {
            users: room.users,
            currentPlayer: room.users[0],
            canRollDice: room.canCurrentPlayerRollDice,
            diceValue: room.currentDiceValue,
            canMoveFigure: room.canCurrentPlayerMoveFigure,
        });

    }, 1000);
};

const rollDice = (roomId, userId, io, socket) => {
    const room = getRoom(roomId);

    const newValue = Math.floor(Math.random() * Math.floor(6) + 1);

    console.log(userId + " rolled " + newValue);
    room.currentDiceValue = newValue;

    socket.to(room._id).emit('currentPlayerRolledDice', {value: newValue});

    //možne poteze
    handlePossibleActions(newValue, roomId, userId, io)
};

const movePlayer = (roomId, userId, playerPosition, io, socket) => {
    const room = getRoom(roomId);
    const userIndex = room.users.findIndex((u) => u._id.toString() === userId.toString());

    if(room.currentPlayer._id !== userId){
        io.to(room._id).emit('error', {
            message: 'You can not move other players figures!'
        });
        return;
    }

    const currentPlayerColor = room.currentPlayer.color;
    const figureIndex = room.currentPlayer.positions.findIndex((p) => p[0] === playerPosition[0] && p[1] === playerPosition[1]);

    if(figureIndex === -1)  {
        io.to(room._id).emit('error', {
            message: 'Can not move. Figure not found!'
        });
        return;
    }

    const newPosition = getNewFigurePosition(room.currentDiceValue, currentPlayerColor, playerPosition, room);

    if(newPosition.error) {
        io.to(room._id).emit('error', {
            message: newPosition.error
        });
    } 
    else if(PlayerHasFigureOnField(room.currentPlayer, newPosition.position) ) {
        io.to(room._id).emit('error', {
            message: 'You can not kick your own figure out!'
        });
    } 
    else {
        KickPlayerFromField(io, room, newPosition.position);
        room.currentPlayer.positions.splice(figureIndex, 1);
        room.currentPlayer.positions.push(newPosition.position);

         //send state to room
        io.to(room._id).emit('gameUpdate', {
            users: room.users,
            currentPlayer: room.users[userIndex],
            canRollDice: false,
            diceValue: room.currentDiceValue,
            canMoveFigure: true,
        });

        setNextPlayer(io, room);
    }
};

//helper functions
const handlePossibleActions = (newValue, roomId, userId, io) => {
    const room = getRoom(roomId);
    const userIndex = room.users.findIndex((u) => u._id.toString() === userId.toString());

    //set figure movement or next player
    if(CanMovePlayer(room.currentPlayer, room.currentDiceValue, room)) {
        console.log("Move figure.");
        
        //send state to room
        io.to(room._id).emit('gameUpdate', {
            users: room.users,
            currentPlayer: room.users[userIndex],
            canRollDice: false,
            diceValue: room.currentDiceValue,
            canMoveFigure: true,
        });
    } else {
        setNextPlayer(io, room);
    }

};

const CanMovePlayer = () => {
    return true;
};

const KickPlayerFromField = (io, room, position) => {
    let playerRemoveIndex = -1;
    let positionRemoveIndex = -1;

    room.users.map((user, userIndex) => {
        user.positions.map((figurePosition , positionIndex) => {
            if(figurePosition[0] === position[0] && figurePosition[1] === position[1]) {
                playerRemoveIndex = userIndex;
                positionRemoveIndex = positionIndex;

                console.log("Kick figure form the field.");
            }
        })
    })

    if(playerRemoveIndex > -1 && positionRemoveIndex > -1){
        let initialPosition = null;
        getInitialsPositions(room.users[playerRemoveIndex].color).map(p => {
            if(!arrayOfPointsIncludes(room.users[playerRemoveIndex].positions, p)){
                initialPosition = p;
            }
        })
        room.users[playerRemoveIndex].positions.splice(positionRemoveIndex, 1);
        room.users[playerRemoveIndex].positions.push(initialPosition);
        io.to(room.id).emit('kickFigureOut', initialPosition);
    }
};

const PlayerHasFigureOnField = (player, position) => {
    return arrayOfPointsIncludes(player.positions, position);
};

const arrayOfPointsIncludes = (array, point) => {
    if(!array || !point) return false;
    let includes = false
    array.map( p => {
        if(p[0] === point[0] && p[1] === point[1]){
            includes = true;
        }
    })
    return includes;
};

const getPathToNewPosition = (startPosition, numOfMoves, playerColor) => {
    let movePath = [];
    //dobimo pot za posameznega igralca
    let playerPath = PlayerPath(playerColor);

    const startIndex = getIndexOfPointInPath(playerPath, startPosition);

    if(startIndex + numOfMoves >= playerPath.length){
        return {
            path: [], 
            error: true
        }
    }

    for(let i = startIndex+1; i <= startIndex+numOfMoves; i++){
        movePath.push(playerPath[i]);
    }

    return {path: movePath, error: false}
};

const getNewFigurePosition = (diceValue, currentPlayerColor, playerPosition, room) => {
    
    //če hočemo dat iz začetne pozicije na polje (moramo vreči 6)
    if(arrayOfPointsIncludes(getInitialsPositions(currentPlayerColor), playerPosition)){
       if(diceValue === 6) {
           return {
               position:getStartPosition(currentPlayerColor), 
               error: null
            }
       } 
       else {
           return {
               position: null,
               error: "Can not move this figure."
            };
       }
   } 
   else {
        //Dobimo novo pozicijo figure na polju
        const result = getPathToNewPosition(playerPosition, diceValue, currentPlayerColor)

        if(result.error){
            return {
                position: null,
                error: "Can not move this figure." 
            };
        } 
        else {
            return {
                position:result.path[result.path.length-1], 
                error: null
            }
        }
   }
};

const getNextPlayer = (room) => {
    let currentIndex = room.users.findIndex((u) => u.username === room.currentPlayer.username);
    currentIndex++;

    if(currentIndex === room.users.length){
        currentIndex = 0;
    }
    
    return room.users[currentIndex];
}

const setNextPlayer = (io, room) => {
    room.currentPlayer = getNextPlayer(room);

    room.canCurrentPlayerRollDice = true;
    room.canCurrentPlayerMoveFigure = false;

    //send state to room
    io.to(room._id).emit('gameUpdate', {
        users: room.users,
        currentPlayer: room.currentPlayer,
        canRollDice: false,
        diceValue: room.currentDiceValue,
        canMoveFigure: true,
    });
};


module.exports = {
    rooms,
    PlayerColor,
    getRoom,
    addUserInRoom,
    removeUserFromRoom,
    startGame,
    removeRoom,
    handleGame,
    getInitialsPositions,
    rollDice,
    movePlayer
} 
