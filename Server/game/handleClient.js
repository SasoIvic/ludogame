const userModel = require('../models/userModel.js');
const {handleGame, rooms, PlayerColor, getRoom, addUserInRoom, startGame, endOfTurn, getInitialsPositions, rollDice, movePlayer} = require('./game.js');
const getTokenData = require('../token.js');

//On new socket connection
const handleConnection = (socket, io) => {

    socket.on('join', ({ roomId, userId }, callback) => {
        console.log(userId + " just joined.");

        userModel.findOne({_id: userId}).exec(function (err, user) {
            if (err) 
                return res.status(500).json({error: "Error getting user from db."});
            if (!user)
                return res.status(500).json({error: "That user does not exist."});

            //add player socket in his room
            socket.join(roomId);

            let room = getRoom(roomId);
            let userIndex;

            //give new user his socketId
            if(room.users){
                userIndex = room.users.findIndex((u) => u._id.toString() === userId.toString());
                room.users[userIndex].socketId = socket.id;
            }

            //start the game if room is full
            if(room.users && room.users.length >= 4){
                console.log("Room is full. Start the game.");

                io.to(roomId).emit('gameStarted');
                handleGame(socket, io, roomId);
            }

            //game started
            if(room.hasGameStarted) {
                handleGame(socket, io, roomId);
                socket.emit('messageUpdate', { sender: "Admin", message: "Welcome " + user.username + ". Enjoy in a game of Ludo."});
            }

        });
    });

    socket.on('startGame', ({roomId, userId}, callback) => {
        let room = getRoom(roomId);

        if(room.counter <= 0){
            console.log("Start the game.");
            handleGame(socket, io, roomId);
            room.counter++;

            if(room.hasGameStarted) socket.emit('messageUpdate', { sender: "Admin", message: "Welcome " + user.username + ". Enjoy in a game of Ludo."});
        }
    });

    socket.on('message', ({ username, message, roomId }, callback) => {
        io.to(roomId).emit('messageUpdate', { sender: username, message: message});
    });

    socket.on('rollDice', ({userId, roomId}, callback) => {
        rollDice(roomId, userId, io, socket);
    })

    socket.on('movePlayer', ({roomId, userId, playerPosition}, callback) => {
        console.log("Move player: " + playerPosition);
        movePlayer(roomId, userId, playerPosition, io, socket);
    })
}

module.exports = {handleConnection}
