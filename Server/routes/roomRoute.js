const express = require('express');
const controller = require('../controllers/roomController.js');
const router = express.Router();
const token = require("../token.js");

router.get('/:id', token.verify, controller.getRoom);
router.get('/', token.verify, controller.getRooms);

router.post('/', token.verify, controller.addRoom);
router.post('/startGame', token.verify, controller.startGame);


router.put('/addPlayerInRoom', token.verify, controller.addPlayerInRoom);
router.put('/removePlayerFromRoom', token.verify, controller.removePlayerFromRoom);
router.put('/:id', token.verify, controller.updateRoom);
router.delete('/:id', token.verify, controller.deleteRoom);

module.exports = router;