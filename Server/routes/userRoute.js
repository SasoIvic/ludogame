const express = require('express');
const controller = require('../controllers/userController.js');
const router = express.Router();

router.get('/:id', controller.getUser);
router.get('/', controller.getUsers);

router.post('/register', controller.registerUser);
router.post('/login', controller.loginUser);


router.put('/:id', controller.updateUser);
router.delete('/:id', controller.deleteUser);

module.exports = router;