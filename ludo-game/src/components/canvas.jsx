import React, {useEffect} from "react";
import CanvasDraw from "react-canvas-draw";
import {createUseStyles} from 'react-jss'


const useStyles = createUseStyles({

    Canvas:{
        width: '80%',
        height: '90%'
    }
    
})

const Canvas = ({canvas, saveDrawingData, sendCanvasData, undo, clear, drawingEnabled}) => {
    const classes = useStyles();

    useEffect(() => {
        canvas.current?.clear();
    }, [clear])

    useEffect(() => {
        canvas.current?.undo();
    }, [undo])

    
    return (
    <div className={classes.Canvas}>    
        <CanvasDraw 
            ref={canvas}
            hideGrid={true}
            style={{ width: '100%', height: '100%' }}
            immediateLoading={true}
            onChange={sendCanvasData}
            saveData={saveDrawingData}
            brushRadius={5}
            lazyRadius={1}
            disabled={!drawingEnabled}
        />
    </div>
    );
}

export default Canvas;
