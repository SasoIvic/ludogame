import React, {useState, useEffect} from "react";
import dice1 from "../images/dice1.png"
import dice2 from "../images/dice2.png"
import dice3 from "../images/dice3.png"
import dice4 from "../images/dice4.png"
import dice5 from "../images/dice5.png"
import dice6 from "../images/dice6.png"
import {createUseStyles} from "react-jss";

const useStyles = createUseStyles({
    disabled: {
        cursor: 'not-allowed',
        opacity: '0.5',
    },
    enabled:{
        cursor: 'pointer'
    },
    diceRolling: {
        animation: 'spin 1s ease-out'
    }
})

const Dice = ({socket, value, currentPlayer}) => {
    const classes = useStyles();

    const [isRolling, setIsRolling] = useState(false);
    const [srcImage, setSrcImage] = useState(dice5);

    useEffect(() => {
        if(value === 1) setSrcImage(dice1);
        if(value === 2) setSrcImage(dice2);
        if(value === 3) setSrcImage(dice3);
        if(value === 4) setSrcImage(dice4);
        if(value === 5) setSrcImage(dice5);
        if(value === 6) setSrcImage(dice6);

        console.log("New dice value: " + value);

    }, [value])



    const rollDice = () => {
        if(currentPlayer && currentPlayer._id == sessionStorage.getItem('userId')){
            let path = window.location.pathname;
            var id = path.split('/')[2];
            setIsRolling(true);
    
            socket.emit('rollDice', { userId: currentPlayer._id, roomId: id});
        }
    }

    return (
        <img
            style={{height: '50px'}}
            alt={"dice"}
            src={srcImage}
            className={currentPlayer && currentPlayer._id != sessionStorage.getItem('userId') ? classes.disabled : classes.enabled + ' ' + isRolling ? classes.diceRolling : ''}
            onClick={rollDice}
        />
    )
}

export default Dice;