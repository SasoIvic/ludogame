import React, {useState} from "react";
import {createUseStyles} from "react-jss";

const useStyles = createUseStyles({
    Figure: {
        height: '40px', 
        width: '40px', 
        color: 'white', 
        fontWeight: 'bold',
        border: '3px solid lightgray',
        margin: 'auto',
        borderRadius: '25px',
    },

})

const Figure = ({color, name, movePlayer}) => {
    const classes = useStyles();

    return (
        <div className={classes.Figure} onClick={movePlayer} style={{backgroundColor: color}}>
            {name}
        </div>
    )
}

export default Figure;