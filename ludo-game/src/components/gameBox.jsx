import React from 'react'
import {createUseStyles} from 'react-jss'
import { TiArrowBack } from 'react-icons/ti';
import { useHistory } from "react-router-dom";
import Dice from './dice';
import Figure from './figure';

const useStyles = createUseStyles({

    DrawBox:{
        height: '100%',
        width: '100%',
        position: 'relative',
        alignItems: 'center'
    },
    TimerBox:{
        position: 'relative',
        height: '5%',
        width: '100%',
        borderTopRightRadius: '30px',
        borderTopLeftRadius: '30px',
        top: '0px',
        left: '0px',
        display: 'flex',
        justifyContent: 'center'

    },
    GameBox:{
        height: '90%',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },

    ToolBox:{
        height: '90%',
        paddingLeft: '20px',
        paddingRight: '20px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    ToolBoxButton:{
        height: '50px',
        width: '50px',
        border: '2px solid #6495ED',
        borderRadius: '30px',
        marginTop: '10px',
        marginBottom: '10px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#6495ED',

        '&:hover':{
            backgroundColor: '#D6EAF7'
        }
    },

    GameGrid: {
        display: "grid",
        gridTemplateColumns: "repeat(11, 1fr)",
        gridTemplateRows: "repeat(11, 1fr)"
    },
})

const filedType = {
    NOT_FIELD: 0,
    RED: 2,
    GREEN: 3,
    BLUE: 4,
    YELLOW: 5,
    FIELD: 1,
    DICE: 6,
}

const gameGrid = [
    [2,2,0,0,1,1,3,0,0,3,3],
    [2,2,0,0,1,3,1,0,0,3,3],
    [0,0,0,0,1,3,1,0,0,0,0],
    [0,0,0,0,1,3,1,0,0,0,0],
    [2,1,1,1,1,3,1,1,1,1,1],
    [1,2,2,2,2,6,5,5,5,5,1],
    [1,1,1,1,1,4,1,1,1,1,5],
    [0,0,0,0,1,4,1,0,0,0,0],
    [0,0,0,0,1,4,1,0,0,0,0],
    [4,4,0,0,1,4,1,0,0,5,5],
    [4,4,0,0,4,1,1,0,0,5,5],
];

const GameBox = ({socket, gameData, diceValue, canMoveFigures, setNotification, showNotification, setShowNotification}) => {
    const classes = useStyles();
    let history = useHistory();

    var canRollDice = true;

    function goBackToDash() {
        history.push("/");
    }

    const movePlayer = (playerPosition) => {
        console.log("move player " + playerPosition);

        let path = window.location.pathname;
        var id = path.split('/')[2];

        socket.emit('movePlayer', {roomId: id, userId: sessionStorage.getItem('userId'), playerPosition: playerPosition}, (err) => {
            if(err){ 
                setNotification(err.error);
                setShowNotification(!showNotification);
                console.log(err);
            }
        });
    }

    return (
        <div className={classes.DrawBox}>
            <div className={classes.TimerBox}>
                <div 
                    className={classes.ToolBoxButton}
                    style={{position: 'absolute', left: '10px', border: 'none'}}
                    onClick={goBackToDash}
                >
                    <TiArrowBack style={{height: '70%', width: '70%', marginRight: '3px'}}/>
                </div>
            </div>

            <div className={classes.GameBox}>
                
                <div className={classes.GameGrid}>
                
                {gameData && gameGrid.map((row, rowIndex) => row.map((field, columnIndex) => {

                    let figure = null;

                    gameData.users.map((player, playerIndex) => 
                    player.positions.map(position => {
                        if(position[0] == rowIndex && position[1] == columnIndex) {
                            if(player.color == "red")
                                figure = <Figure key={playerIndex} color={player.color} name={player.username.substring(0, 2).toUpperCase()} movePlayer={()=>movePlayer(position)}/>
                            else if (player.color == "green")
                                figure = <Figure key={playerIndex} color={player.color} name={player.username.substring(0, 2).toUpperCase()} movePlayer={()=>movePlayer(position)}/>
                            else if (player.color == "yellow")
                                figure = <Figure key={playerIndex} color={player.color} name={player.username.substring(0, 2).toUpperCase()} movePlayer={()=>movePlayer(position)}/>
                            else if (player.color == "blue")
                                figure = <Figure key={playerIndex} color={player.color} name={player.username.substring(0, 2).toUpperCase()} movePlayer={()=>movePlayer(position)}/>
                        }
                    }))

                    if(field === filedType.RED){
                        return (<div key={rowIndex + columnIndex} className={classes.filed} style={{backgroundColor: 'red', height: '50px', width: '50px'}}> {figure} </div>)
                    } else if(field === filedType.BLUE){
                       return (<div key={rowIndex + columnIndex} className={classes.filed} style={{backgroundColor: 'blue', height: '50px', width: '50px'}}> {figure} </div>)
                    } else if(field === filedType.YELLOW){
                       return (<div key={rowIndex + columnIndex} className={classes.filed} style={{backgroundColor: 'orange', height: '50px', width: '50px'}}> {figure} </div>)
                    } else if(field === filedType.GREEN){
                       return (<div key={rowIndex + columnIndex} className={classes.filed} style={{backgroundColor: 'green', height: '50px', width: '50px'}}> {figure} </div>)
                    } else if(field === filedType.FIELD){
                        return (<div key={rowIndex + columnIndex} className={classes.filed} style={{backgroundColor: 'white', height: '50px', width: '50px', border: '1px solid #F8F8F8'}}> {figure} </div>)
                    } else if(field === filedType.DICE){
                        return (
                            <div key={rowIndex + columnIndex} style={{backgroundColor: 'black', height: '50px', width: '50px', borderRadius: '2px'}}>
                            <Dice
                                socket={socket}
                                value={diceValue}
                                currentPlayer={gameData.currentPlayer}
                            />
                            </div>
                        )
                    } else {
                       return (<div key={rowIndex + columnIndex} />)
                    }
                }))}

                </div>

            </div>

        </div>
    )
}

export default GameBox;
