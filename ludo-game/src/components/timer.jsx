import React from 'react'
import {createUseStyles} from 'react-jss'

const useStyles = createUseStyles({

    Timer:{
        backgroundColor: '#D6EAF7',
        borderBottomRightRadius: '15px',
        borderBottomLeftRadius: '15px',
        width: 'fit-content',
        height: 'fit-content',
        padding: '10px 25px 10px 25px',
        color: '#6495ED',
        fontWeight: 'bold',
        fontSize: '20px'
    },
})

const Timer = ({timeCountDown}) => {
    const classes = useStyles();
  
    return (
      <div className={classes.Timer}>
          {timeCountDown && timeCountDown}
      </div>
    );
  }

  export default Timer;
