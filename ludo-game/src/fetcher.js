import axios from "axios";


export const httpGet = (url, callback) => {
    let token = sessionStorage.getItem('token');
    axios({
        method: 'get',
        url: url,
        headers: token ? {
            'Authorization': token
        } : null,
    }).then(res =>{
        callback(res.data);
    }).catch(err => {
        console.log(err)
    });
}

export const httpPost = (url, data, callback, onError) => {
    let token = sessionStorage.getItem('token');
    axios({
        method: 'post',
        url: url,
        data: data,
        headers: token ? {
            'Authorization': token
        } : null,
    }).then(res =>{
        if(callback) callback(res.data);
    }).catch(err => {
        if(onError) onError(err);
        console.log(err)
    });
}

export const httpPut = (url, data, callback) => {
    let token = sessionStorage.getItem('token');
    axios({
        method: 'put',
        url: url,
        data: data,
        headers: token ? {
            'Authorization': token
        } : null,
    }).then(res =>{
        callback(res.data);
    }).catch(err => {
        console.log(err);
        return err;
    });
}

export const httpDelete = (url, callback) => {
    let token = sessionStorage.getItem('token');
    axios({
        method: 'delete',
        url: url,
        headers: token ? {
            'Authorization': token
        } : null,
    }).then(res =>{
        callback(res.data);
    }).catch(err => {
        console.log(err)
    });
}
