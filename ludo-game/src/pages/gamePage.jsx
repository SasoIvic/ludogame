import React, {useEffect, useState, useRef} from 'react'
import {createUseStyles} from 'react-jss'
import GameBox from '../components/gameBox'
import ChatBox from '../components/chatBox'
import { useHistory } from "react-router-dom";
import useSound from 'use-sound';
import wonSound from '../won.mp3';

import io from "socket.io-client"

const SERVER_URL = "http://127.0.0.1:8000/"

const useStyles = createUseStyles({
    GameWrapper:{
        width: '100%',
        height: '100%',
        display: 'grid',
        gridTemplateColumns: '3fr 1fr',
        gridGap: '20px',
        backgroundColor: '#6495ED',
        position: 'relative',
    },
    DrawBox:{
        margin: '20px 0px 20px 20px',
        backgroundColor: '#EBF5FB',
        borderRadius: '30px',
    },
    ChatBox:{
        margin: '20px 20px 20px 0px',
        backgroundColor: '#EBF5FB',
        borderRadius: '30px'
    },

    Notification:{
        position: 'absolute',
        left: '40%',
        top: '5%',
        width: '650px',
        height: '50px',
        marginTop: '-25px',
        marginLeft: '-325px',
        fontSize: '3.5vh',
        color: '#6495ED',
        fontWeight: '600',
        textTransform: 'uppercase',
        backgroundColor: 'rgba(250, 250, 250, 0.95);',

    },

})

var socket;

const GamePage = props => {
    const classes = useStyles();
    let history = useHistory();

    const [messages, setMessages] = useState([]);
    const [diceValue, setDiceValue] = useState(5);
    const [currentPlayer, setCurrentPlayer] = useState(null);
    const [canMoveFigures, setCanMoveFigures] = useState(false);
    const [gameData, setGameData] = useState(null);
    const [notification, setNotification] = useState(null);
    const [showNotification, setShowNotification] = useState(false);

    useEffect(() => {
        
        let path = window.location.pathname;
        var id = path.split('/')[2];

        socket = io(SERVER_URL);

        //join
        socket.emit('join', { roomId: id, userId: sessionStorage.getItem('userId') }, (data) => {
            if(data.error != null) {
                alert(data.error);
            }
        });

        //start game
        socket.emit('startGame', { roomId: id, userId: sessionStorage.getItem('userId') }, (data) => {
            console.log("startGame");
        });
 
        //chat messages
        socket.on('messageUpdate', message => {
            setMessages(messages => [ ...messages, message ]);
        });

        //get winner of turn and points
        socket.on('gameOver', data => {      
            console.log("Game over");
        });

        socket.on('error', err => {
            setNotification(err.message);
            setShowNotification(true);

            setTimeout(function(){ setShowNotification(false); }, 5000);
        });

        //update game
        socket.on('gameUpdate', data => {

            console.log("Game update: ");
            console.log(data);

            setCurrentPlayer(data.currentPlayer);
            setGameData(data);

            if(!data.canMoveFigure) {
                setCanMoveFigures(false);
            } 
            else if(sessionStorage.getItem('username') === data.currentPlayer.username && data.canMoveFigure) {
                setCanMoveFigures(true);
            }

            setDiceValue(data.diceValue);
        });

        //get new dice value
        socket.on('currentPlayerRolledDice', data => {
            console.log("Dice value: " + data.value);
            setDiceValue(data.value);
        });

        return () => socket.disconnect();

    }, [])

    return (
        <div className={classes.GameWrapper}>
            <div className={classes.Notification} style={{zIndex: !showNotification ? -1 : 100}}>{showNotification && notification && notification}</div>
            <div className={classes.DrawBox}>
                <GameBox 
                    socket={socket} 
                    gameData={gameData} 
                    diceValue={diceValue} 
                    canMoveFigures={canMoveFigures} 
                    setNotification={setNotification}
                    setShowNotification={setShowNotification}
                    showNotification={showNotification}
                />
            </div>
            <div className={classes.ChatBox}>
                <ChatBox socket={socket} messages={messages}/>
            </div>
        </div>
    )
}

export default GamePage;
