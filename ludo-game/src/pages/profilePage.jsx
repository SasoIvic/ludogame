import React, {useState, useEffect} from 'react'
import {createUseStyles} from 'react-jss'
import { useHistory } from "react-router-dom";
import { TiArrowBack } from 'react-icons/ti';
import { BiTrophy } from 'react-icons/bi';
import {httpGet} from '../fetcher';


const SERVER_URL = "http://127.0.0.1:8000/"


const useStyles = createUseStyles({
    WaitingRoom:{
        width: '100%',
        height: '100%',
        backgroundColor: '#6495ED',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'cursive',

    },
    Container:{
        backgroundColor: '#EBF5FB',
        borderRadius: '30px',
        width: '30%',
        height: '80%',
        margin: '50px',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        position: 'relative',
        
        '& h2':{
            color: '#6495ED',
            textTransform: 'uppercase',
            fontSize: '8vh',
            marginBottom: '20px',
            marginTop: '20px',
            textShadow: '1.25px 1.25px #12449f',
        },
        '& h4':{
            color: '#f1d78b',
            fontSize: '3vh',
            textShadow: '1.25px 1.25px #908153',
        },
        '& p':{
            margin: '5px',
            fontSize: '3vh',
            color: '#6495ED',
        },
    },
    
    ToolBoxButton:{
        position: 'absolute',
        top: '10px',
        left: '15px',
        height: '50px',
        width: '50px',
        border: '2px solid #6495ED',
        borderRadius: '30px',
        marginTop: '10px',
        marginBottom: '10px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#6495ED',

        '&:hover':{
            backgroundColor: '#D6EAF7'
        }
    },

    Row:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: 	'#6495ED',
        fontSize: '2vh'
    }
})

const Profile = props => {
    const classes = useStyles();
    let history = useHistory();

    function goBackToDash() {
        history.push("/");
    }

    const [user, setUser] = useState(null);

    useEffect(() => {

        httpGet(SERVER_URL + `user/` + sessionStorage.getItem('userId'), data => {
            setUser(data.user);
        });
    }, [])

    return (
        <div className={classes.WaitingRoom}>
            <div className={classes.Container}>

                <div 
                    className={classes.ToolBoxButton}
                    onClick={goBackToDash}
                >
                    <TiArrowBack style={{height: '70%', width: '70%', marginRight: '3px'}}/>
                </div>
                
                <BiTrophy style={{color: '#F1CE8B', height: '80px', width: '80px'}}/>

                <h2>{user && user.username}</h2>

                <div className={classes.Row}>
                    <>Number of games played: </>
                    <>{user && user.gamesPlayedNum}</>
                </div>

                <div className={classes.Row}>
                    <>Number of wins: </>
                    <>{user && user.winsNum}</>
                </div>
                
                <div className={classes.Row}>
                    <>Total points: </>
                    <>{user && user.points}</>
                </div>

            </div>
        </div>
    )
}

export default Profile;
